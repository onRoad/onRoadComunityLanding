export enum UrlByProjectEnum {
    frontend = 'https://gitlab.com/onRoad/onRoadFrontend',
    api = 'https://gitlab.com/onRoad/onRoadAPI',
    comunityLanding = 'https://gitlab.com/onRoad/onRoadComunityLanding',
}
