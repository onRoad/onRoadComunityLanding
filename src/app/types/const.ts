export enum constants {
    TEAM_IMAGES_URL = './../assets/images/team/',
    ONROAD_OFFICIAL_EMAIL = 'onroadcompany@gmail.com',
    LANDING_STANDAR_SUBJECT = 'onRoadComunityLanding - User Opinion',
    MAILER_SERVICE_URL = 'http://localhost:8087/email/send',
}
