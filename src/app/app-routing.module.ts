import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { ContactComponent } from './components/contact/contact.component';
import { TeamComponent } from './components/team/team.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { FaqsComponent } from './components/faqs/faqs.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'team', component: TeamComponent },
    { path: 'projects', component: ProjectsComponent },
    { path: 'faqs', component: FaqsComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            { /*enableTracing: true*/ } // <-- debugging purposes only
        )
    ], exports: [
        RouterModule
    ]
})

export class AppRoutingModule { }
