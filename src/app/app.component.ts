import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';

  @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;

  constructor(private _toastrService: ToastrService) { }

  ngOnInit() {
    this._toastrService.overlayContainer = this.toastContainer;
  }

}
