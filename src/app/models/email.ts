export class Email {
    constructor(
        public to?: string,
        public subject?: string,
        public body?: string) { }
}
