import { Component, OnInit } from '@angular/core';
import { UrlByProjectEnum } from '../../types/projects-enum';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public newWindow(projectId: string) {
    console.log(UrlByProjectEnum[projectId]);

    window.open(
      UrlByProjectEnum[projectId],
      '_blank' // <- This is what makes it open in a new window.
    );

  }

}
