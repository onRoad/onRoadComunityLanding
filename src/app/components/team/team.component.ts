import { Component, OnInit } from '@angular/core';
import { constants } from './../../types/const';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  private _members: Object[];

  constructor() {
  }

  ngOnInit() {
    this._members = [
      {
        name: 'Iván',
        occupation: 'Full Stack developer',
        jobAtOnRoad: 'CEO',
        picture: constants.TEAM_IMAGES_URL + 'ivan.png',
        comment: 'Join to the family!'
      },
      {
        name: 'Cristian',
        occupation: 'Senior Full Stack developer',
        jobAtOnRoad: 'CTO',
        picture: constants.TEAM_IMAGES_URL + 'cristian.png',
        comment: 'Are u onRoad?'
      },
      {
        name: 'Claudia',
        occupation: 'Student',
        jobAtOnRoad: 'Head of design team',
        picture: constants.TEAM_IMAGES_URL + 'claudia.png',
        comment: 'Hello!'
      },
      {
        name: 'Efra',
        occupation: 'Free artist',
        jobAtOnRoad: 'Illustrator',
        picture: constants.TEAM_IMAGES_URL + 'efra.png',
        comment: 'Shup'
      }
    ];
  }

}
