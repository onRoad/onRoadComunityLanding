import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CheckboxControlValueAccessor } from '@angular/forms/src/directives/checkbox_value_accessor';
import { EmailService } from '../../services/email/email.service';
import { Email } from '../../models/email';
import { constants } from '../../types/const';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  providers: [EmailService]
})
export class ContactComponent implements OnInit {

  public isLinear: boolean;
  public opinionFormGroup: FormGroup;
  public infoFormGroup: FormGroup;

  public userComment: string;
  public userName: string;
  public userEmail: string;
  public userWannaJoin: boolean;
  public userSkills: any = {};

  private email: Email;

  constructor(private _formBuilder: FormBuilder, private _emailService: EmailService, private _toastr: ToastrService) {
    this.email = new Email();
  }

  ngOnInit() {
    this.isLinear = true;

    this.opinionFormGroup = this._formBuilder.group({
      opinionCtrl: ['', Validators.required]
    });

    this.userWannaJoin = false;

    this.infoFormGroup = this._formBuilder.group({
      nameCtrl: ['', Validators.required],
      emailCtrl: ['', Validators.email]
    });
  }

  sendEmail(): void {
    this.email.to = constants.ONROAD_OFFICIAL_EMAIL;
    this.email.subject = constants.LANDING_STANDAR_SUBJECT;
    this.email.body = this.generateEmailBody();
    this._emailService.sendEmail(this.email).subscribe(
      response => this._toastr.success('Thank you very much', 'Sent'),
      error => this._toastr.error('Seems like something went wrong', 'Error')
    );
  }

  private generateEmailBody(): string {
    let emailTemplate = `<p><b>${this.userName}</b> says:</p>`;
    emailTemplate += `<p><em>${this.userComment}</em></p>`;

    if (this.userWannaJoin) {
      emailTemplate += `<p>The user want to help as:<ul>`;

      if (this.userSkills.isProgrammer) {
        emailTemplate += `<li>Programmer</li>`;
      }
      if (this.userSkills.isDesigner) {
        emailTemplate += `<li>Designer</li>`;
      }
      if (this.userSkills.isOthers) {
        emailTemplate += `<li>Others</li>`;
      }

      emailTemplate += `</ul></p>`;
    }

    emailTemplate += `<p><b>${this.userName}</b> email is: <b>${this.userEmail}</b></p>`;
    return emailTemplate;
  }
}
