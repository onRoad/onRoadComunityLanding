import { Injectable } from '@angular/core';
import { Email } from './../../models/email';
import { constants } from '../../types/const';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class EmailService {

  constructor(private _http: HttpClient) { }

  sendEmail(email: Email): Observable<any> {
    return this._http.post(constants.MAILER_SERVICE_URL, JSON.stringify(email), httpOptions);
  }
}
