import { NgModule } from '@angular/core';
import { MatButtonModule, MatCheckboxModule, MatMenuModule, MatIconModule, MatCard } from '@angular/material';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatStepperModule } from '@angular/material/stepper';
import { FormsModule, ReactiveFormsModule, } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';

@NgModule({
  imports: [MatButtonModule, MatCheckboxModule, MatMenuModule, MatTabsModule,
    MatCardModule, MatGridListModule, MatStepperModule, FormsModule, ReactiveFormsModule,
    MatFormFieldModule, MatInputModule, MatIconModule],
  exports: [MatButtonModule, MatCheckboxModule, MatMenuModule, MatTabsModule,
    MatCardModule, MatGridListModule, MatStepperModule, FormsModule, ReactiveFormsModule,
    MatFormFieldModule, MatInputModule, MatIconModule],
})
export class MaterialDesignModule { }
